<?php
/**
 * @author     Onetree <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Onetree_RemoveUnusedMedia', __DIR__);
