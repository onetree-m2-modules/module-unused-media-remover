# Onetree_RemoveUnusedMedia module

This module can list or delete the unused catalog media files that are not used or required by definition.

### Usage

`bin/magento media:remove:unused <options>`

#### Options
`--action (-a)` values can be `delete` or `info`. Indicates what action to take. This option is MANDATORY
+ `info` only shows information about founded media files
+ `delete` deletes non-used files

\
`--log (-l)` values can be `1` or `0`. Indicates if the command have to write logs, log file is placed in `var/log/console.removed-media.log`


### Paths
`pub/media/catalog/products` is the only analyzed path

If you want to exclude more sub-folders, add them in `EXCLUDED_DIRS` constant.
