<?php

namespace Onetree\RemoveUnusedMedia\Model;
use Magento\Framework\Logger\Handler\Debug;
use Monolog\Logger;

class CustomLogger extends Debug
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/console.removed-media.log';

    /**
     * @var int
     */
    protected $loggerType = Logger::INFO;
}
