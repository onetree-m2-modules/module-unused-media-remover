<?php
/**
 * @author  Juan Pablo Müller   Onetree <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */


namespace Onetree\RemoveUnusedMedia\Console;

use Laminas\Code\Exception\BadMethodCallException;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\App\ResourceConnection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\MissingInputException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveUnusedMedia extends Command
{
    const PARAM_ACTION = [
        'name' => 'action',
        'shortcut' => 'a',
        'description' => 'Indicates wich action to take (info|delete)',
        'options' => [
            'info',
            'delete'
        ]
    ];
    const PARAM_LOG = [
        'name' => 'log',
        'shortcut' => 'l',
        'description' => 'Save execution log, true by default',
        'default' => 0,
        'options' => [
            0,
            1
        ]
    ];

    /**
     * Excluded directories over base path
     */
    const EXCLUDED_DIRS = [
        'cache',
        'placeholder'
    ];

    /**
     *  Base path
     */
    const MEDIA_FOLDER = 'pub/media/catalog/product';

    /**
     * @var ResourceConnection
     */
    protected ResourceConnection $_resource;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var int
     */
    private int $loggingOption;

    /**
     * @param ResourceConnection $resource
     * @param LoggerInterface $logger
     */
    public function __construct(
        ResourceConnection $resource,
        LoggerInterface    $logger,
    ) {
        $this->_resource = $resource;
        $this->logger = $logger;
        parent::__construct('media:remove:unused');

    }

    /**
     * @return void
     */
    protected function configure(): void {

        $options = [
            new InputOption(
                self::PARAM_ACTION['name'],
                self::PARAM_ACTION['shortcut'],
                InputOption::VALUE_REQUIRED,
                self::PARAM_ACTION['description'],
            ),
            new InputOption(
                self::PARAM_LOG['name'],
                self::PARAM_LOG['shortcut'],
                InputOption::VALUE_OPTIONAL,
                self::PARAM_LOG['description'],
                self::PARAM_LOG['default'],
            )
        ];

        $this->setName('media:remove:unused')
            ->setDescription('Remove unused media')
            ->setDefinition($options);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {

        //Prepare params
        $action = $input->getOption(self::PARAM_ACTION['name']);
        if (!$action) {
            throw new MissingInputException('Action parameter is required');
        }
        if (!in_array($action, self::PARAM_ACTION['options'])) {
            throw new MissingInputException('Action parameter must be one of (' . implode(', ', self::PARAM_ACTION['options']) . ')');
        }

        $log = $input->getOption(self::PARAM_LOG['name']);
        if (!in_array($log, self::PARAM_LOG['options'])) {
            throw new MissingInputException('Log paramenter must be one of (' . implode(', ', self::PARAM_ACTION['options']) . ')');
        }
        $this->loggingOption = $log;

        //Execute actions
        $realPath = realpath(self::MEDIA_FOLDER);
        $this->log('Working over path: ' . $realPath);


        //Get existing files from defined directory
        $fileList = $this->getFilesInDir(self::MEDIA_FOLDER);
        $fileList = array_map(function ($file) use ($realPath) {
            return str_replace($realPath, '', $file);
        }, $fileList);
        $this->log("Found " . count($fileList) . " files in selected path");


        //Get database linked image paths
        $connection = $this->_resource->getConnection();
        $usedMedia = $connection->fetchCol("SELECT value FROM " . Gallery::GALLERY_TABLE);
        $usedMedia = array_unique($usedMedia);
        $this->log("Found " . count($usedMedia) . " different values in " . Gallery::GALLERY_TABLE . " table");

        //Unused files;
        $diff = array_diff($fileList, $usedMedia);
        $this->log(count($diff) . " files exists but aren't used by " . Gallery::GALLERY_TABLE . " table");

        //Required that not exists
        $diff2 = array_diff($usedMedia, $fileList);
        $this->log(count($diff2) . " called files by " . Gallery::GALLERY_TABLE . " table that not exists");


        if (count(self::EXCLUDED_DIRS)) {
            $output->writeln("<comment>Excluded paths:</comment>");
            foreach (self::EXCLUDED_DIRS as $exdir) {
                $output->writeln("- " . $realPath . DIRECTORY_SEPARATOR . $exdir);
            }
            $output->writeln('');
        }
        if ($action === 'info') {
            $output->writeln('* Existing files: ' . count($fileList));
            $output->writeln('* Not used existing files: ' . count($diff));
            $output->writeln('* Required files: ' . count($usedMedia));
            $output->writeln('* Required that not exists: ' . count($diff2));
            return 1;
        }

        if (count($diff) > 0) {
            $start = microtime(true);
            $output->writeln("Starting unused file deletion. " . count($diff) . " files");

            $this->log("Starting unused file deletion. " . count($diff) . " files");
            foreach ($diff as $fileToDelete) {
                $fileToDelete = $realPath . $fileToDelete;
                try {
                    unlink($fileToDelete);
                    if ($input->getOption('verbose')) {
                        $output->writeln("<info>Deleted:</info> " . $fileToDelete);
                    }
                    $this->log("Deleted " . $fileToDelete);
                } catch (\Exception $e) {
                    $output->writeln('<error>Error deleting:</error> ' . $fileToDelete);
                    $this->log("Error deleting: " . $fileToDelete, 'error');
                }

            }
            $end = microtime(true);
            $output->writeln("Done in " . number_format($end - $start, 4) . " seconds... ");
        } else {
            $output->writeln("There's nothing to do ＼(＾O＾)／ ");
        }

        return 1;
    }

    /**
     * @param $dir
     * @param array $results
     * @return array
     */
    private function getFilesInDir($dir, array &$results = array()): array {
        $files = scandir($dir);

        foreach ($files as $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                if ($this->isExcluded($path)) {
                    continue;
                }
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getFilesInDir($path, $results);
            }
        }

        return $results;
    }

    /**
     * @param string $path
     * @return bool
     */
    private function isExcluded(string $path): bool {
        foreach (self::EXCLUDED_DIRS as $dir) {
            if (str_contains($path, self::MEDIA_FOLDER . DIRECTORY_SEPARATOR . $dir)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param mixed $params
     * @param string $logLevel
     * @return void
     */
    private function log(mixed $params, string $logLevel = 'info'): void {
        if ($this->loggingOption == 1) {
            if (method_exists($this->logger, $logLevel)) {
                $this->logger->$logLevel(is_string($params) ? $params : json_encode($params));
                return;
            }
            throw new BadMethodCallException('Log type not exists: ' . $logLevel);
        }
    }
}
